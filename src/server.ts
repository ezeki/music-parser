
import * as radio from './read-stream';
import { api, HttpCodes } from './common/api'
import { pubsub, Events } from './common/pubsub'

class Server {

    async start() {
        const res = await api.get<any>('/graphql?query={items:musicChannels(limit: 1000, includeOfflines: true){ id, source }}');
        if (!res || res.statusCode != HttpCodes.OK) return;
        const items = res.result.data.items;

        for (const item of items) {
            this.startReadingMetadata(item.id, item.source);
        }

        console.log(`Started ${items.length}`);
    }

    private startReadingMetadata(id, url) {
        var stream = radio.createReadStream(url);

        stream.on("metadata", function (metadata) {

            var title = radio.parseMetadata(metadata).StreamTitle;
            if (!title || title == '') return;


            const payload = {
                channelId: id,
                info: title
            }

            pubsub.publish(Events.MusicChannel.TrackUpdated, JSON.stringify({ musicChannelTrackUpdated: payload }));
        });

        stream.on("error", async function (info) {

            // TODO Implement in x.jok.io
            try {
                await api.update('/Music/ChannelOffline/', { channelID: id });
            }
            catch (err) {
            }
            // $.post(API_ROOT_URL + '/Music/ChannelOffline/?channelID=' + id);
        });
    }
}

new Server().start();