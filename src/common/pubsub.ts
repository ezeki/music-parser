import * as Redis from 'ioredis';

export const Events = {
    MusicChannel: {
        TrackUpdated: 'MusicChannel.TrackUpdated'
    }
}

const REDIS_CONFIG = 'redis-18107.c2.eu-west-1-3.ec2.cloud.redislabs.com:18107:E5kizdrOHDFt7eb8'

const REDIS_DOMAIN_NAME = REDIS_CONFIG.split(':')[0];
const REDIS_PORT_NUMBER = +REDIS_CONFIG.split(':')[1];
const REDIS_PASSWORD = REDIS_CONFIG.split(':')[2];

export const pubsub = new Redis({
    port: REDIS_PORT_NUMBER,
    host: REDIS_DOMAIN_NAME,
    // family: 4,           // 4 (IPv4) or 6 (IPv6)
    password: REDIS_PASSWORD,
});
