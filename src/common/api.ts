import { RestClient } from "typed-rest-client/RestClient";
export { HttpCodes } from "typed-rest-client/HttpClient";

export const api = new RestClient('Jok.MusicParser', 'https://jokio-graph.herokuapp.com/');